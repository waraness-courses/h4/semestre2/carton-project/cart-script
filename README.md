# Cart script

This script use a our custom pre-trained yolo v5 model to detect products inside a specific cart and update the Firestore database accordingly

## To get started

First install all dependencies.

```
pip install -r requirements.txt
```

Then get serviceAccountKey.json from Firebase and put it at the repository root.
