import torch
import cv2

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import threading
from datetime import datetime


def init_webcam():
    video_capture = cv2.VideoCapture(0)
    # Set webcam resolution
    video_capture.set(3, 1280)
    video_capture.set(4, 720)
    cv2.namedWindow("Window")

    return video_capture


def init_model():
    """"Load our custom pretrained yolo v5 model"""
    model = torch.hub.load('ultralytics/yolov5', 'yolov5s')
    model = torch.hub.load('ultralytics/yolov5', 'custom', path='./best.pt')

    return model


def init_cart_doc():
    """Initialize firestore db and get cart document"""
    cred = credentials.Certificate("./serviceAccountKey.json")
    firebase_admin.initialize_app(cred)
    db = firestore.client()
    return db.collection(u'carts').document(u'GFi1KRdzltlWcdAhBh1n')


def on_cart_change(doc_snapshot, changes, read_time):
    """Print changes when cart changed on firestore db"""
    for doc in doc_snapshot:
        print(f'Received document snapshot: { doc.to_dict()}')
    callback_done.set()


def get_product_quantities(raw_cart_products):
    """Return quantities of each product contained in the cart"""
    quantities = {}

    for product in raw_cart_products:
        confidence = product['confidence']
        product_name = product['name']

        if confidence > 0.5:
            quantities[product_name] = quantities.get(product_name, 0) + 1

    return quantities


def get_cart_object(cart_quantity):
    """Construct and return a cart object ready to send to firestore db"""

    cart = {'articles': []}

    for key, value in cart_quantity.items():
        cart['articles'].append({
            'reference': key,
            'quantity': value,
        })

    return cart


# Initialize global variables
cart_doc = init_cart_doc()
webcam = init_webcam()
model = init_model()

last_upload = datetime.now()
callback_done = threading.Event()
cart_doc_watch = cart_doc.on_snapshot(on_cart_change)


while True:
    ret, frame = webcam.read()
    # Crop webcam frame to a centered square of 720p
    frame = frame[:, 280:1000]
    cv2.imshow("Window", frame)

    results = model(frame)
    # Create de dataframe from result
    df = results.pandas().xyxy[0]
    # Convert dataframe to dict
    raw_data = df.to_dict(orient='records')

    product_quantities = get_product_quantities(raw_data)

    # Each 2 seconds, update cart on firebase db
    elapsed_time = (datetime.now() - last_upload).total_seconds()
    if elapsed_time > 2:
        cart_object = get_cart_object(product_quantities)
        cart_doc.set(cart_object)
        last_upload = datetime.now()

    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


webcam.release()
cv2.destroyAllWindows()
